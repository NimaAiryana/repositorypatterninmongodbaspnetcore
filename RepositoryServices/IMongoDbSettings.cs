namespace repositorypatterninmongodbaspnetcore.RepositoryServices
{
    public interface IMongoDbSettings
    {
        string DatabaseName { get; set; }
        string ConnectionString { get; set; }
    }
}