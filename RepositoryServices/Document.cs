using System;
using MongoDB.Bson;

namespace repositorypatterninmongodbaspnetcore.RepositoryServices
{
    public abstract class Document : IDocument
    {
        public ObjectId Id { get; set; }

        public DateTime CreatedAt => Id.CreationTime;
    }
}