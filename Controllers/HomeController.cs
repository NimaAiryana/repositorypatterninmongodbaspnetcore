using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using repositorypatterninmongodbaspnetcore.Models;
using repositorypatterninmongodbaspnetcore.RepositoryServices;

namespace repositorypatterninmongodbaspnetcore.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<Person> personRepository;

        public HomeController(IRepository<Models.Person> personRepository)
        {
            this.personRepository = personRepository;
        }

        public IActionResult Index()
        {
            personRepository.InsertOne(new Person()
            {
                FirstName = "nima",
                LastName = "airyana",
                BirthDate = DateTime.UtcNow
            });

            var list = personRepository.FilterBy(person => true).ToList();

            return Content(System.Text.Json.JsonSerializer.Serialize(list));
        }
    }
}